# raspi-k8s-nfssetup
Shell script for setting up an [NFS server](https://hub.docker.com/r/itsthenetwork/nfs-server-alpine/) inside a Raspberry PI Kubernetes cluster with [dynamic storage provisioning](https://hub.helm.sh/charts/stable/nfs-client-provisioner).

# Prerequisites
A PI cluster setup with [raspi-k8s-nfssetup](http://bitbucket.org/sequlo/raspi-k8s-nfssetup). You probably want to mount some storage device on a node inside the cluster. To automatically mount the storage device on boot you have to edit `/etc/fstab`. The partition on your storage device you wish to mount might be formatted in one of many file system formats. I would recommend you format the target pratition as ext4 or some other filesystem where Linux file permissions and ownership will be preserved. I started out with NTFS only to find out that is was not able to preserve these kind of properties. You would als have to install a third-party storage driver like `ntfs-3g` to be able to mount. A Raspberry PI with Raspbian has native support for the well known Linux filesystems ext3 and ext4, so If you're starting out with a fresh storage device and don't care about formatting the thing, then do so.

On the node where you want to mount the storage device, use a tool like `lsblk` or `blkid` to find out the device's file descriptor e.g. `/dev/sda`. Use a tool like `fdisk` to alter partition tables and you can also format the partition as ext4 by running something like:
```bash
sudo mkfs.ext4 /dev/sda1
```
Now make a new directory where you want the partition (e.g. `/mnt/hdd`) and add the following line to `/etc/fstab` to be able to mount the partition on boot:
```bash
/dev/sda1 /mnt/hdd ext4 defaults,user,noatime,nofail 0 0
```
Use `mount -a -v` the see your addition to `/etc/fstab` realized on a new mountpoint. All the tooling mentioned so far is already onboard Raspbian so no need to install anything.

# How to Run
Login on the the master node and execute the following steps.

## Step 1. Configure nfssetup
```bash
sudo curl -o /etc/nfssetup.conf https://bitbucket.org/sequlo/raspi-k8s-nfssetup/raw/HEAD/example.nfssetup.conf
sudo vi /etc/nfssetup.conf
``` 

## Step 2. Run nfssetup
```bash
sudo curl -o /usr/local/sbin/nfssetup https://bitbucket.org/sequlo/raspi-k8s-nfssetup/raw/HEAD/nfssetup
sudo nfssetup
```
