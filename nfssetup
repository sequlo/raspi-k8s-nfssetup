#!/bin/bash
#
# Setup dynamic storage provisioning via NFS on a Raspberry PI Kubernetes cluster

set -o errexit

function continue_step() {
  declare -r id="$1"
  if [ -f "${STATE_DIR}/step_${id}.done" ]; then
    echo "Skip ${id}"
    return 1
  fi
  echo "Do ${id}"
  return 0
}

function mark_step_done() {
  declare -r id="$1"
  touch "${STATE_DIR}/step_${id}.done"
  echo "${id} done!"
}

function install_nfs_server_on_worker() {
  continue_step "${FUNCNAME[0]}" || return
  sudo -u "$ADMIN_USERNAME" ssh "${DEV_HOSTNAME}" docker run -d --restart=always --name=nfs-server --privileged -p 2049:2049 -v "${DEV_MOUNTPOINT}":/nfsshare -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:11-arm
  mark_step_done "${FUNCNAME[0]}"
}

function mount_nfs_share_on_master() {
  continue_step "${FUNCNAME[0]}" || return
  mkdir -p '/mnt/nfs'
  chown -R "$(id -u $ADMIN_USERNAME):$(id -g $ADMIN_USERNAME)" '/mnt/nfs'
  echo "${DEV_HOSTIP}:/ /mnt/nfs nfs rw 0 0" >> '/etc/fstab'
  mount -v -a
  mkdir -p '/mnt/nfs/k8s'
  mark_step_done "${FUNCNAME[0]}"
}

function install_nfs_client_provisioner() {
  continue_step "${FUNCNAME[0]}" || return
  sudo -u "$ADMIN_USERNAME" kubectl config set-context --current kube-system 
  sudo -u "$ADMIN_USERNAME" helm install nfs-client-provisioner stable/nfs-client-provisioner --set nfs.server="${DEV_HOSTIP}" --set nfs.path='/k8s' --set image.repository=quay.io/external_storage/nfs-client-provisioner-arm
  sudo -u "$ADMIN_USERNAME" kubectl config set-context --current default 
  mark_step_done "${FUNCNAME[0]}"
}

function main() {
  if [ "$(id -u)" != '0' ]; then
    echo 'This script must be run as root' 1>&2
    exit 1
  fi
  declare -r basename="$(basename "$0")"
  declare -r log_file="/var/log/${basename}.log"
  exec > >(tee -ia "$log_file")
  exec 2>&1
  echo "Run: $(date '+%Y-%m-%d %H:%M:%S')"
  declare -r conf_file="/etc/${basename}.conf"
  if [ ! -s "$conf_file" ]; then
    echo "Missing configuration file: $conf_file" 1>&2
    exit 1
  fi
  source "$conf_file"
  declare -rg STATE_DIR="/var/lib/$basename"
  declare -rg DEV_HOSTIP="$(getent hosts ${DEV_HOSTNAME} | awk '{ print $1 }' )"
  mkdir -p "$STATE_DIR"
  install_nfs_server_on_worker
  mount_nfs_share_on_master
  install_nfs_client_provisioner
}

main "$@"
